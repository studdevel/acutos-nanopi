#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp> 
#include <opencv2/features2d/features2d.hpp>
#include <memory>
#include <cstdint>

using namespace cv;
using namespace std;

#ifndef OPENCVHELPER_H
#define OPENCVHELPER_H

struct Color
{
    Scalar filterLow1;
    Scalar filterHigh1;
    Scalar filterLow2;
    Scalar filterHigh2;
};

class OpenCVHelper 
{
public:
    OpenCVHelper();
    Mat compute(Color color);
    Mat getFrame();
    OpenCVHelper(const OpenCVHelper& other);
    virtual ~OpenCVHelper();
    Mat treshold1;
private:
    void initialize();

private:
    shared_ptr<VideoCapture> _capture;
};

#endif 

