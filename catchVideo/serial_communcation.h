#include <iostream>
#include <string>
#include <cstring>
#include <fcntl.h>
#include <termios.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

class Serial_communication 
{
public:
    Serial_communication(std::string paramPathSerial);
    ~Serial_communication();
    int openPort();
    int sendPort(std::string testString);
    int closePort();
private:
    std::string pathSerial;
    //filedescriptors
    int fd;
};