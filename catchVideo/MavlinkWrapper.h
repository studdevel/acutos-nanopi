#include "OpenCVHelper.h"
#include <mavlink.h>
#include <mavlink_types.h>
#include "serial_communcation.h"

class MavlinkWrapper 
{
public:
    MavlinkWrapper(uint8_t system_id, uint8_t component_id, std::string serial_dev, uint8_t m_type);
    ~MavlinkWrapper();
    uint8_t component_id;
    uint8_t system_id;
    void sendMessage(mavlink_message_t &msg);
    void createHeartbeat();
    Serial_communication *sc;

    std::string serial_dev;
    uint8_t m_type;
};