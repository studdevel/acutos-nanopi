#include "MavlinkWrapper.h"

MavlinkWrapper::MavlinkWrapper(uint8_t system_id, uint8_t component_id, std::string serial_dev, uint8_t m_type = 2)
{
    this->system_id = system_id;
    this->component_id = component_id;
    this->serial_dev = serial_dev;
    this->m_type = m_type;
    sc = new Serial_communication(serial_dev);
    sc->openPort();
}

MavlinkWrapper::~MavlinkWrapper()
{
    delete sc;
}

void MavlinkWrapper::sendMessage(mavlink_message_t &msg)
{
    uint8_t buffer[MAVLINK_MAX_PACKET_LEN];
    int lenght = mavlink_msg_to_send_buffer(buffer, &msg);
    if (!lenght) return;
    std::string temp(reinterpret_cast<char const*>(buffer), MAVLINK_MAX_PACKET_LEN);
    sc->sendPort(temp);
}

void MavlinkWrapper::createHeartbeat()
{
    mavlink_message_t message;
    mavlink_heartbeat_t heartbeat;
    heartbeat.type = m_type;
    mavlink_msg_heartbeat_encode(system_id, component_id, &message, &heartbeat);
    sendMessage(message);
}