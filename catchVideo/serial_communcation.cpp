#include <iostream>

#include "serial_communcation.h"

Serial_communication::Serial_communication(std::string paramPathSerial)
{
    fd = 0;
    pathSerial = paramPathSerial;
}

Serial_communication::~Serial_communication()
{
}

int Serial_communication::openPort()
{
    std::cout << "openPort() [serial_communication] started with the following paramters... pathSerial = " << pathSerial << ", baudrate = 19200" << std::endl;

    //OPENING PORT

    //open serial port
    fd = open(pathSerial.c_str(), O_RDWR | O_NOCTTY);
    if(fd < 0)
    {
        std::cout << "Error [serial_communcation]: opening Port: " << pathSerial << std::endl;
        return -1;
    }

    //struct termios
    termios serial;

    memset(&serial, 0, sizeof(struct termios));

    serial.c_cflag = CS8|CREAD|PARENB|CLOCAL;
    /* Set Baud Rate */
    // cfsetospeed (&serial, (speed_t)baudrate);
    // cfsetispeed (&serial, (speed_t)baudrate);

    cfsetispeed (&serial, B19200);

    /* Make raw */
    cfmakeraw(&serial);

    /* Flush Port, then applies attributes */
    tcflush( fd, TCIFLUSH );

    //set attributes to port
    if(tcsetattr(fd, TCSANOW, &serial) < 0)
    {
        std::cout << "Error [serial_communication]: set attributes" << std::endl;
        return -1;
    }

    //CONFIGURATION FINISHED
    std::cout << "openPort() [serial_communication] finished..." << std::endl;
    return 1;
}

int Serial_communication::sendPort(std::string textString)
{
    std::cout << "write() [Serial_communication] started with the following parameter... textString = " << textString << std::endl;
    //attempt to send
    if(write(fd, &textString, std::strlen(textString.c_str())) < 0)
    {
        std::cout << "Error [serial_communcation]: write";
        return -1;
    }

    //SENDING FINISHED
    std::cout << "write() [serial_communcation] finished..." << std::endl;
    return 1;
}

int Serial_communication::closePort()
{
    close(fd);
    return 1;
}